package ru.coderiders.iostreams.lab2;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;

public class LaunchLabTwo {
    public static void main(String[] args) throws IOException {
        var scanner = new Scanner(System.in);
        System.out.println("Enter path to XML file: ");
        var xml = Files.readString(new File(scanner.next()).toPath()
                , StandardCharsets.US_ASCII);
        System.out.println("Enter path where to save JSON file: ");
        var json = new File(scanner.next());
        json.createNewFile();
        var parsed = XmlToJsonParser.parse(xml);
        Files.write(json.toPath(), List.of(parsed.split("\n")), StandardCharsets.UTF_8);
        System.out.println("JSON:\n" + parsed);
    }
}
