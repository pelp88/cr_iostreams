package ru.coderiders.iostreams.lab2;

import org.json.*;

public class XmlToJsonParser {
    static String parse(String xmlContents){
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xmlContents);
            return xmlJSONObj.toString(3);
        } catch (JSONException e) {
            throw new JSONException(e);
        }
    }
}
