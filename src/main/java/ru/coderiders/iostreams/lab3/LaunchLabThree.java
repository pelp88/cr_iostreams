package ru.coderiders.iostreams.lab3;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Scanner;

public class LaunchLabThree {
    public static void main(String[] args) throws IOException {
        var scanner = new Scanner(System.in);
        System.out.println("Enter path to first text file: ");
        var string1 = Files.readString(new File(scanner.next()).toPath()
                , StandardCharsets.US_ASCII);
        System.out.println("Enter path to second text file: ");
        var string2 = Files.readString(new File(scanner.next()).toPath()
                , StandardCharsets.US_ASCII);
        var difference = StringDifference.diff(string1, string2);
        System.out.println("Difference:\n" + difference);
    }
}
