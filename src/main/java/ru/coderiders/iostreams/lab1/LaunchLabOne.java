package ru.coderiders.iostreams.lab1;

import java.io.IOException;
import java.util.Scanner;

public class LaunchLabOne {
    public static void main(String[] args) throws IOException {
        var scanner = new Scanner(System.in);
        System.out.println("Enter file name/content (partially or full): ");
        var name = scanner.next();
        System.out.println("Enter path: ");
        var path = scanner.next();
        System.out.println("By name: " + Finder.findFileByName(name, path));
        System.out.println("By contents: " + Finder.findFileByContents(name, path));
    }
}
