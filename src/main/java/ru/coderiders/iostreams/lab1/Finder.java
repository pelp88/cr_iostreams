package ru.coderiders.iostreams.lab1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Collection;
import java.util.stream.Collectors;

public class Finder {
    static Collection<Path> findFileByName(String file, String directory) throws IOException {
        try (var files = Files.walk(Paths.get(directory))) {
            return files.filter(f -> f.getFileName().toString().contains(file))
                        .collect(Collectors.toList());

        }
    }

    static Collection<Path> findFileByContents(String content, String directory) throws IOException {
        try (var files = Files.walk(Paths.get(directory))) {
            return files.filter(f -> {
                        try {
                            return Files.readString(f, StandardCharsets.UTF_8)
                                            .contains(content)
                                            && Files.probeContentType(f).contains("text/plain");
                        } catch (IOException ignored) {}
                        return false;
                    })
                    .collect(Collectors.toList());

        }
    }
}