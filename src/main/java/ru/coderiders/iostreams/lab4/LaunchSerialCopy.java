package ru.coderiders.iostreams.lab4;

import java.util.Scanner;

public class LaunchSerialCopy {
    public static void main(String[] args) throws Exception {
        var scanner = new Scanner(System.in);
        System.out.println("Enter first file path: ");
        var from1 = scanner.next();
        System.out.println("Enter second file path: ");
        var from2= scanner.next();
        System.out.println("Enter first file dest path: ");
        var to1 = scanner.next();
        System.out.println("Enter second file dest path: ");
        var to2= scanner.next();

        FileCopy.io_copy(from1, to1);
        FileCopy.io_copy(from2, to2);
    }
}
