package ru.coderiders.iostreams.lab4;

import java.io.*;
import java.nio.file.*;

public class FileCopy {
    public static void io_copy(String from, String to) throws Exception {
        File original = new File(from);
        File copied = new File(to);
        try (
                var in = new BufferedInputStream(
                        new FileInputStream(original));
                var out = new BufferedOutputStream(
                        new FileOutputStream(copied))) {

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public static void nio_copy(String from, String to) throws Exception {
        var fromPath = Paths.get(from);
        var toPath = Paths.get(to);
        Files.copy(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING);
    }
}
