package ru.coderiders.iostreams.lab4;

import java.io.File;
import java.util.Scanner;

public class LaunchParallelCopy {
    public static void main(String[] args){
        var scanner = new Scanner(System.in);
        System.out.println("Enter first file path: ");
        var from1 = scanner.next();
        System.out.println("Enter second file path: ");
        var from2= scanner.next();
        System.out.println("Enter first file dest path: ");
        var to1 = scanner.next();
        System.out.println("Enter second file dest path: ");
        var to2= scanner.next();

        new Thread(() -> {
            try {
                FileCopy.io_copy(from1, to1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                FileCopy.io_copy(from2, to2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
